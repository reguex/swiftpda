import Foundation

public enum AcceptanceMethod {
  case acceptanceState
  case emptyStack
}
