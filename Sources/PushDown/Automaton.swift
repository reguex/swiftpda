import Foundation

public class Automaton<StateType:EntityType, InputType:EntityType, StackElement:EntityType> {
  public typealias TransitionInputType = TransitionInput<InputType>
  public typealias TransitionType = Transition<StateType, TransitionInputType, StackElement>

  private typealias StepType = Step<StateType, InputType, StackElement>

  public private(set) var states: Set<StateType>
  public private(set) var acceptanceStates: Set<StateType>
  public private(set) var initialState: StateType?
  public private(set) var transitions: Set<TransitionType>
  public private(set) var alphabet: Set<InputType>
  public private(set) var initialStackInput: StackElement?
  public var acceptanceCriteria: AcceptanceMethod

  public init() {
    self.states = []
    self.acceptanceStates = []
    self.initialState = nil
    self.transitions = []
    self.alphabet = []
    self.initialStackInput = nil
    self.acceptanceCriteria = .acceptanceState
  }

  public func addState(_ state: StateType) {
    states.insert(state)
  }

  public func addAcceptanceState(_ state: StateType) {
    addState(state)
    acceptanceStates.insert(state)
  }

  public func setInitialState(_ state: StateType) {
    addState(state)
    initialState = state
  }

  public func setInitialStackSymbol(_ symbol: StackElement) {
    initialStackInput = symbol
  }

  public func addSymbol(_ symbol: InputType) {
    alphabet.insert(symbol)
  }

  public func addTransition(_ transition: TransitionType) {
    addState(transition.origin)
    addState(transition.destiny)

    if case let .value(value) = transition.input {
      addSymbol(value)
    }

    transitions.insert(transition)
  }

  public func addTransition(
      from origin: StateType,
      to destiny: StateType,
      on input: TransitionInputType,
      pop: StackElement,
      push: [StackElement]
  ) {
    addTransition(Transition(origin: origin, input: input, pop: pop, push: push, destiny: destiny))
  }

  public func addTransition(
      from origin: StateType,
      to destiny: StateType,
      on input: InputType,
      pop: StackElement,
      push: [StackElement]
  ) {
    addTransition(from: origin, to: destiny, on: .value(input), pop: pop, push: push)
  }

  public func addEpsilonTransition(
      from origin: StateType,
      to destiny: StateType,
      pop: StackElement,
      push: [StackElement]
  ) {
    addTransition(from: origin, to: destiny, on: .epsilon, pop: pop, push: push)
  }

  public func transitions(from origin: StateType, on input: TransitionInputType, popped: StackElement)
          -> Set<TransitionType> {
    return transitions.filter { $0.origin == origin && $0.input == input && $0.pop == popped }.asSet
  }

  public func removeAcceptanceState(_ state: StateType) {
    acceptanceStates.remove(state)
  }

  public func removeState(_ state: StateType) {
    removeAcceptanceState(state)
    if initialState == state {
      initialState = nil
    }

    for transition in transitions where transition.origin == state || transition.destiny == state {
      transitions.remove(transition)
    }

    states.remove(state)
  }

  public func removeSymbol(_ symbol: InputType) {
    for transition in transitions where transition.input == .value(symbol) {
      transitions.remove(transition)
    }
    alphabet.remove(symbol)
  }

  public func languageContains(_ word: [InputType], verbose: Bool = false) throws -> Bool {
    guard let initialState = initialState else { throw PdaError.UndefinedInitialState }
    guard let initialStackInput = initialStackInput else { throw PdaError.UndefinedInitialStackInput }

    let initialStep = StepType(currentState: initialState, word: Array(word), stack: [initialStackInput])
    var foundSteps = Set<StepType>([initialStep])
    var unprocessedSteps = [initialStep]

    while let currentStep = unprocessedSteps.popLast() {

      if matchesAcceptanceCriteria(step: currentStep) {
        return true
      }

      let branchedSteps = branchStep(currentStep)

      if verbose {
        print("Current Step: \(currentStep)")
      }

      for branchedStep in branchedSteps where foundSteps.doesNotContain(branchedStep) {
        if case .emptyStack = acceptanceCriteria,
           branchedStep.stack.count > word.count {
          continue
        }

        unprocessedSteps.append(branchedStep)
        foundSteps.update(with: branchedStep)
      }
    }

    if verbose {
      print("Branched: \(foundSteps.count)")
    }

    return false
  }

  private func branchStep(_ step: StepType) -> Set<StepType> {
    var branchedSteps = Set<StepType>()

    if let readInput = step.word.first {
      let readInputSteps = branchStep(step, on: .value(readInput))
      branchedSteps.formUnion(readInputSteps)
    }

    let epsilonSteps = branchStep(step, on: .epsilon)
    branchedSteps.formUnion(epsilonSteps)

    return branchedSteps
  }

  private func branchStep(_ step: StepType, on input: TransitionInputType) -> Set<StepType> {
    guard let popped = step.stack.first else { return [] }

    var nextWord = step.word
    if case .value = input {
      nextWord.removeFirst()
    }

    var branchedSteps = Set<StepType>()

    let valueMoves = transitions(from: step.state, on: input, popped: popped)
    for move in valueMoves {
      var nextStack = Array(step.stack.dropFirst())
      nextStack.insert(contentsOf: move.push, at: 0)

      let newStep = StepType(currentState: move.destiny, word: nextWord, stack: nextStack)
      branchedSteps.insert(newStep)
    }

    return branchedSteps
  }

  private func matchesAcceptanceCriteria(step: StepType) -> Bool {
    guard step.word.isEmpty else {
      return false
    }

    switch acceptanceCriteria {
    case .emptyStack:
      return step.stack.isEmpty
    case .acceptanceState:
      return acceptanceStates.contains(step.state)
    }
  }
}

extension Automaton: CustomStringConvertible {
  public var description: String {
    var text = "Automaton {" + "\n\t"
    text += "States: \(states)" + "\n\t"
    text += "Alphabet: \(alphabet)" + "\n\t"
    text += "Acceptance States: \(acceptanceStates)" + "\n\t"
    text += "Initial State: \(initialState.map(String.init(describing:)) ?? "")" + "\n\t"
    text += "Initial Stack Input: \(initialStackInput.map(String.init(describing:)) ?? "")" + "\n\t"
    text += "Transitions {" + "\n\t\t\t"
    text += transitions.map { $0.description }.joined(separator: "\n\t\t\t") + "\n\t"
    text += "}"
    return text
  }
}