import Foundation

public enum PdaError: Error {
  case UndefinedInitialState
  case UndefinedInitialStackInput
}