import Foundation

extension Automaton where InputType == StackElement {
  public convenience init(stateValue q: StateType, rules: [Rule<StackElement>]) {
    self.init()

    let productionRuleVariables = Set(rules.map { $0.symbol })

    self.acceptanceCriteria = .emptyStack
    self.setInitialState(q)
    self.addAcceptanceState(q)

    if let initialStackSymbol = rules.first?.symbol {
      self.setInitialStackSymbol(initialStackSymbol)
    }

    for rule in rules {
      addTransition(from: q, to: q, on: .epsilon, pop: rule.symbol, push: rule.production)

      for productionInput in rule.production where productionRuleVariables.doesNotContain(productionInput) {
        addTransition(from: q, to: q, on: .value(productionInput), pop: productionInput, push: [])
      }
    }
  }
}

extension Automaton {
  public func grammar(initialGrammarInputName: String) throws -> [Rule<String>] {
    guard let initialState = self.initialState else { throw PdaError.UndefinedInitialState }
    guard let initialStackInput = self.initialStackInput else { throw PdaError.UndefinedInitialStackInput }

    var productions = [Rule<String>]()

    let initialProductionStates: Set<StateType>
    switch acceptanceCriteria {
    case .emptyStack: initialProductionStates = states
    case .acceptanceState: initialProductionStates = acceptanceStates
    }

    for state in initialProductionStates {
      let newProduction = ["\(initialState)|\(initialStackInput)|\(state)"]
      productions.append(Rule(symbol: initialGrammarInputName, production: newProduction))
    }

    for transition in transitions where transition.push.isEmpty {
      let newVariable = "\(transition.origin)|\(transition.pop)|\(transition.destiny)"
      let newProduction = ["\(transition.input)"]
      productions.append(Rule(symbol: newVariable, production: newProduction))
    }

    for transition in transitions where transition.push.isNotEmpty {
      let stateProduct = Array(product(states, repeats: transition.push.count))
      for r in stateProduct {
        let newVariable = "\(transition.origin)|\(transition.pop)|\(r.last!)"
        var newProduction: [String] = []
        if case .value = transition.input {
          newProduction.append(transition.input.description)
        }
        newProduction.append("\(transition.destiny)|\(transition.push[0])|\(r[0])")
        for i in 0..<(transition.push.count - 1) {
          newProduction.append("\(r[i])|\(transition.push[i + 1])|\(r[i + 1])")
        }
        productions.append(Rule(symbol: newVariable, production: newProduction))
      }
    }

    return productions
  }
}