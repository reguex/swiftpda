import Foundation

public struct Rule<Element> {
  public let symbol: Element
  public let production: [Element]
}

extension Rule: CustomStringConvertible {
  public var description: String {
    return "\(symbol) -> \(production)"
  }
}
