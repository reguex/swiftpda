import Foundation

extension Sequence where Self.Iterator.Element: Hashable {
  var reducedHash: Int {
    return self.reduce(0) { $0 ^ $1.hashValue }
  }
}

extension Sequence {
  func any(_ predicate: (Self.Iterator.Element) throws -> Bool) rethrows -> Bool {
    for element in self {
      let result = try predicate(element)
      if result {
        return true
      }
    }
    return false
  }

  func all(_ predicate: (Self.Iterator.Element) throws -> Bool) rethrows -> Bool {
    for element in self {
      let result = try predicate(element)
      if !result {
        return false
      }
    }
    return true
  }

  func none(_ predicate: (Self.Iterator.Element) throws -> Bool) rethrows -> Bool {
    let result = try self.any(predicate)
    return !result
  }

  func count(_ predicate: (Self.Iterator.Element) throws -> Bool) rethrows -> Int {
    var count = 0
    for element in self {
      if try predicate(element) {
        count += 1
      }
    }
    return count
  }

  func filterNot(_ predicate: (Self.Iterator.Element) throws -> Bool) rethrows -> [Self.Iterator.Element] {
    var filtered: Array<Self.Iterator.Element> = []
    for item in self where try (!predicate(item)) {
      filtered.append(item)
    }
    return filtered
  }
}

extension Sequence where Self.Iterator.Element: Equatable {
  func containsAny<S:Sequence>(from other: S) -> Bool where S.Iterator.Element == Self.Iterator.Element {
    return other.any(self.contains)
  }

  func containsAll<S:Sequence>(in other: S) -> Bool where S.Iterator.Element == Self.Iterator.Element {
    return other.all(self.contains)
  }

  func containsNone<S:Sequence>(in other: S) -> Bool where S.Iterator.Element == Self.Iterator.Element {
    return !containsAny(from: other)
  }

  func doesNotContain(_ element: Self.Iterator.Element) -> Bool {
    return !self.contains(element)
  }
}

extension Sequence where Self.Iterator.Element: Hashable {
  var asSet: Set<Self.Iterator.Element> {
    return Set(self)
  }
}

extension Collection {
  var isNotEmpty: Bool {
    return !self.isEmpty
  }
}

public struct product<T>: IteratorProtocol, Sequence {
  private var indices : [Int]
  private var pools : [[T]]
  private var done : Bool = false

  public init(_ arrays: [[T]]) {

    self.pools = arrays
    self.indices = [Int](repeating: 0, count: self.pools.count)
    self.done = pools.filter({$0.count == 0}).count != 0
  }

  public init<S:Sequence>(_ array: S, repeats: Int) where S.Iterator.Element == T {
    assert(repeats >= 0, "repeats must be >= 0")

    self.pools = [[T]](repeating: Array(array), count: repeats)
    self.indices = [Int](repeating: 0, count: self.pools.count)
    self.done = repeats <= 0 && pools.filter({$0.count == 0}).count != 0
  }

  public mutating func next() -> [T]? {
    if done {
      return nil
    }

    let element = pools.enumerated().map {
      $1[ self.indices[$0] ]
    }

    self.incrementLocationInPool(poolIndex: self.pools.count-1)
    return element
  }

  mutating private func incrementLocationInPool(poolIndex: Int) {
    if poolIndex < 0 {
      return done = true
    }

    indices[poolIndex] += 1

    if indices[poolIndex] == pools[poolIndex].count {
      indices[poolIndex] = 0
      incrementLocationInPool(poolIndex: poolIndex-1)
    }
  }

  public func generate() -> product {
    return self
  }
}