internal struct Step<StateType:EntityType, InputType:EntityType, StackElement:EntityType> {
  public let state: StateType
  public let word: [InputType]
  public let stack: [StackElement]

  internal init(currentState: StateType, word: [InputType], stack: [StackElement]) {
    self.state = currentState
    self.word = word
    self.stack = stack
  }
}

extension Step: Hashable {
  public var hashValue: Int {
    return state.hashValue ^ word.reducedHash ^ stack.reducedHash &* 65435
  }

  public static func ==(
      lhs: Step<StateType, InputType, StackElement>,
      rhs: Step<StateType, InputType, StackElement>
  ) -> Bool {
    return lhs.state == rhs.state && lhs.word == rhs.word && lhs.stack == rhs.stack
  }
}

extension Step: CustomStringConvertible {
  public var description: String {
    return "(\(state), \(word), \(stack))"
  }
}