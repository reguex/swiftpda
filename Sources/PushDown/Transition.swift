import Foundation

public struct Transition<StateType:EntityType, InputType:EntityType, StackElement:EntityType> {
  public let origin: StateType
  public let input: InputType
  public let pop: StackElement
  public let push: [StackElement]
  public let destiny: StateType

  internal init(origin: StateType, input: InputType, pop: StackElement, push: [StackElement], destiny: StateType) {
    self.origin = origin
    self.input = input
    self.pop = pop
    self.push = push
    self.destiny = destiny
  }
}

extension Transition: CustomStringConvertible {
  public var description: String {
    return "(\(origin), \(input), \(pop)) -> (\(destiny), \(push.isNotEmpty ? push.description : "ε"))"
  }
}

extension Transition: Hashable {
  public var hashValue: Int {
    return origin.hashValue ^ input.hashValue ^ pop.hashValue ^ destiny.hashValue ^ push.reducedHash &* 64533
  }

  public static func ==(
      lhs: Transition<StateType, InputType, StackElement>,
      rhs: Transition<StateType, InputType, StackElement>
  ) -> Bool {
    return lhs.origin == rhs.origin && lhs.input == rhs.input && lhs.pop == rhs.pop &&
        lhs.destiny == rhs.destiny && lhs.push == rhs.push
  }
}