import Foundation

public enum TransitionInput<InputType:EntityType> {
  case epsilon
  case value(InputType)

  func unlessEpsilon(do action: (InputType) throws -> ()) rethrows {
    if case let .value(value) = self {
      try action(value)
    }
  }
}

extension TransitionInput: CustomStringConvertible {
  public var description: String {
    switch self {
    case .epsilon: return "ε"
    case .value(let value): return String(describing: value)
    }
  }
}

extension TransitionInput: Hashable {
  public var hashValue: Int {
    switch self {
    case .epsilon: return 1
    case .value(let value): return value.hashValue
    }
  }

  public static func ==(lhs: TransitionInput<InputType>, rhs: TransitionInput<InputType>) -> Bool {
    switch(lhs, rhs) {
    case (.epsilon, .epsilon): return true
    case (.value(let a), .value(let b)): return a == b
    default: return false
    }
  }
}