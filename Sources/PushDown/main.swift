import Foundation

let pda = Automaton<String, String, String>(stateValue: "q", rules: [
  Rule(symbol: "E", production: ["E", "+", "T"]),
  Rule(symbol: "E", production: ["E", "-", "T"]),
  Rule(symbol: "E", production: ["T"]),
  Rule(symbol: "T", production: ["T", "*", "F"]),
  Rule(symbol: "T", production: ["T", "/", "F"]),
  Rule(symbol: "T", production: ["F"]),
  Rule(symbol: "F", production: ["digit"]),
  Rule(symbol: "digit", production: ["0"]),
  Rule(symbol: "digit", production: ["1"])
])

let grammar: [Rule<String>] = try pda.grammar(initialGrammarInputName: "S")
let newPda = Automaton<String, String, String>(stateValue: "q", rules: grammar)

NSLog(newPda.description)
try NSLog(pda.languageContains("1+1-1-1----1-1/1-0".characters.map { String($0) }, verbose: false).description)